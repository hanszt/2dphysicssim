## Tools for testing and code quality
PMD: Static source code analyser
ArchTest: Testing layered architecture of your application
![Clean-architecture](clean-architecture.png)