package hzt.service;

import hzt.model.Resource;

import java.util.Set;

public interface IBackgroundService {

    Set<Resource> getResources();
}
